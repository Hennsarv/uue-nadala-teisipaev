﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipäevPikalt
{
    class ProtokolliRida
    {
        public string Nimi;
        public int Distants;
        public TimeSpan Aeg;
        public double Kiirus;

        public override string ToString()
        {
            return $"{Nimi} jooksis {Distants} meetrit ajaga {Aeg}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<ProtokolliRida> read = new List<ProtokolliRida>();

            foreach (string rida in System.IO.File.ReadLines(@"..\..\spordipäeva protokoll.txt"))
            {
                if (rida.Trim() != "")
                    if (rida.Substring(0, 4) != "Nimi")
                    {
                        var osad = rida.Split(',');
                        for (int i = 0; i < osad.Length; i++)
                            osad[i] = osad[i].Trim(); ;

                        read.Add(new ProtokolliRida { Nimi = osad[0], Distants = int.Parse(osad[1]) });
                    }

                foreach (var x in read)
                {
                    Console.WriteLine(x);
                }

                


            }
            foreach (var r in read.ToLookup(x => x.Nimi, x => x))
            {
                Console.WriteLine($"Meil oli sportlane {r.Key}");
                foreach (var j in r)
                    Console.WriteLine($"\ttema distants: {j.Distants} ajaga {j.Aeg}");
            }
        }
    }
}
