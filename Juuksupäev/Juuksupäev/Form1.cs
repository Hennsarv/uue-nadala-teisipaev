﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Juuksupäev
{
    

    public partial class Form1 : Form
    {
        private List<Rida> LoetudRead = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var loetudRead = System.IO.File.ReadAllLines(@"..\..\spordipäeva protokoll.txt");
            
            this.LoetudRead = loetudRead
                .Skip(1)
                .Where(x => x.Trim() != "")
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                
                .Select(x => new Rida { Nimi = x[0], Distants = x[1] })
                .ToList();

            this.dataGridView1.DataSource = LoetudRead.Select(x => new { x.Nimi }).ToList();
            this.button2.Visible = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (LoetudRead != null)
            {
                LoetudRead = LoetudRead.OrderBy(x => x.Nimi).ToList();
                this.dataGridView1.DataSource = LoetudRead;
            }
        }
    }
}
