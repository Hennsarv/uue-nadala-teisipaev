﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpoleeritudString
{
    class Inimene : IFormattable // et saaks anda ToStringile formaati
                                 // implementeerin IFormattable interface
    {
        public string Nimi { get; set; }
        public string Isikukood { get; private set; }

        public Inimene(string isikukood) => Isikukood = isikukood;

        public override string ToString()
        => $"Inimene {Nimi}";

        // lisan funktsiooni ToString parameetritega
        public string ToString(string format, IFormatProvider formatProvider)
        {
            //if (format == "i") return $"Inimene {Nimi} isikukoodiga {Isikukood}";
            //else return ToString();
            return format == "i" ? ToString()
                + " isikukoodiga "
                + Isikukood.ToString() : ToString();
        }

        // kui mul on palju erinevaid formaate, siis teen umbes nii

        public string ToString(string format)
        {
            //switch (format)
            //{
            //    case "i":
            //        return ToString() + $" isikukoodiga {Isikukood}";
            //    case "nn": return $"isikukood {Isikukood}";
            //    default: return ToString();

            //}
            if (format == "i") return ToString() + $" isikukoodiga {Isikukood}";
            //else
                if (format == "nn") return $"isikukood {Isikukood}";
            //else
                return ToString();
        }



        // lihtsuse mõttes teine veel (overloaditud) - ilma teise parameetrita
        //public string ToString(string format) => ToString(format, null);
    }

    class PalgaTase
    {
        public double Piir { get; set; }
        public string Kirjeldus { get; set; }
    }

    static class Program
    {
        static PalgaTase[] Tasemed = null;

        static string PalgaTase(this double palk)
        {
            if (palk > 1000) return "kõrge";
            if (palk > 500) return "normaalne";
            if (palk > 100) return "keskmine";
            if (palk > 10) return "pisike";
            return "puudub";
        }



        static string Palgatase2(this double palk)
        {
            if (Tasemed == null)
            Tasemed =
            //{
            //    new PalgaTase {Piir = 1000, Kirjeldus = "kõrge"},
            //    new PalgaTase {Piir = 500, Kirjeldus = "normaalne"},
            //    new PalgaTase {Piir = 100, Kirjeldus = "keskmine"},
            //    new PalgaTase {Piir = 10, Kirjeldus = "pisike"},
            //    new PalgaTase {Piir = 0, Kirjeldus = "puudub"}

            //};
            System.IO.File.ReadAllLines(@"..\..\Palgatasemed.txt")
            .Select(x => new PalgaTase { Piir = double.Parse(x.Split(',')[0]), Kirjeldus = x.Split(',')[1] })
            .ToArray();
            foreach (var x in Tasemed)
                if (palk > x.Piir) return x.Kirjeldus;
            return "null";
        }

        static void Main(string[] args)
        {
            string nimi = "Henn";
            int vanus = 63;

            // string kokkupanemine + tehtega
            Console.WriteLine(nimi + " on " + vanus.ToString() + " aastane");
            string vastus = nimi + " on " + vanus.ToString() + " aastane";
            Console.WriteLine(vastus);

            // string format koos placeholderitega (kohatäitjatega)
            vastus = string.Format("{0} on {1} aastane", nimi, vanus);
            Console.WriteLine(vastus);

            // see on nüüd sama tehe interpoleeritud stringiga
            vastus = $"{nimi} on {vanus} aastane";
            Console.WriteLine(vastus);

            // natukene ToString meetodist
            // ToStringi formaadimuster - parameeter, kuidas teisendada
            double palk = 10.0 / 3;
            Console.WriteLine(palk.ToString("f2"));

            // placeholderisse ToStringi formaadi lisamine
            Console.WriteLine("{0} saab {1} eurot palka", nimi, palk.ToString("f2"));
            Console.WriteLine("{0} saab {1:f2} eurot palka", nimi, palk);

            Console.WriteLine(DateTime.Now.ToString(@"dddd dd.MMMM.yyyy kella\d on hh:mm:ss.fff"));

            // interpoleeritud stringi formaadi lisamine
            vastus = $"{nimi} saab {palk:f2} eurot palka";
            Console.WriteLine(vastus);

            //string katse = palk < 400 ? "jubevähe" : palk.ToString("f2");
            //  see on koht, kus sulud (                                             )  
            vastus = $"{nimi} palk on {(palk < 400 ? "jubevähe" : palk.ToString("f2"))}";
            Console.WriteLine(vastus);
            // interpoleeritud stringis avaldises ?: või stringi "" kasutamisel tuleb
            // panna avaldis SULGUDESSE ()
            // seda rida ma kahjuks ei saa treppida, kuna STRINGI ei saa treppida

            Inimene henn = new Inimene("35503070211") { Nimi = "Henn Sarv" };
            Console.WriteLine(henn);
            Console.WriteLine(henn.ToString("i"));
            Console.WriteLine($"{henn:i} saab {palk:f2} eurot");

            //if (palk > 1000)
            //{
            //    if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            //    Console.WriteLine("läheme pralletama");
            //}
            //else if (palk > 100)
            //{
            //    Console.WriteLine("läheme kinno");

            //}
            //else if (palk > 10)
            //    Console.WriteLine("nosime kotletti saiaga");
            //else
            //    Console.WriteLine("ei tee midagi");

            palk = 10001;
            Console.WriteLine(palk.Palgatase2());

        }
    }
}
