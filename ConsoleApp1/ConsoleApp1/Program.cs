﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{


    static class Program
    {
        
        public  static List<T> ToList<T>(this IEnumerable<T> source)
        {
            List<T> tulemus = new List<T>();
            foreach (var x in source) tulemus.Add(x);
            return tulemus;
        }

        public static IEnumerable<T> Where<T>(this IEnumerable<T> source, Func<T,bool> predicate)
        {
            foreach (T x in source)
                if (predicate(x))
                    yield return x;
        }

        public static IEnumerable<T> Skip<T>(this IEnumerable<T> source, int n)
        {
            int loendur = 0;
            foreach (var x in source) if (++loendur > n) yield return x;
        }

        public static IEnumerable<T> Take<T>(this IEnumerable<T> source, int n)
        {
            int loendur = 0;
            foreach (var x in source)
                if (++loendur > n) break;
                else yield return x;
            {

            }
            {

            }
            {

            }
        }

        // see on extension (this teeb ta selliseks)
        // kuna näiliselt ta nagu laiendaks int klassi
        public static int TõstaRuutu(this int x)
        {
            return x * x;
        }

        // see on samasugune funktsioon, aga lühemalt
        public static int TõstaKuupi(this int x)
            => x * x * x;

        public static void Trüki(this int x , string nimi)
        {
            Console.WriteLine($"{nimi}: {x}");
        }

        public static void Trüki2(this int x, string nimi)
            => Console.WriteLine($"{nimi}: {x}");


        static void Main(string[] args)
        {
            Console.WriteLine(TõstaRuutu(7));

            int a = 17;
            Console.WriteLine(  (3 + 9).TõstaRuutu());
            Console.WriteLine( TõstaRuutu(3 + 9));

            // kaks erinevat meetodi väljakutset
            Trüki(3 + 9, "summa");
            (3+9).Trüki("summa");

            Func<int, int> ruuduke = x => x * x + a;
            Func<int, int, int> summake = (int x, int y) => x + y;

            ruuduke(4).Trüki("neljake");

            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            List<int> paarisarvud = new List<int>();
            foreach (var x in arvud)
            {
                if (x % 2 == 0) paarisarvud.Add(x);
            }
          

            paarisarvud = new List<int>();
            foreach (var x in arvud.Where(ž => ž % 2 == 0))
            {
                paarisarvud.Add(x);
            }

            paarisarvud = arvud.Where<int>(x => x % 2 == 0).ToList();

            paarisarvud.Sum();

        }
    }
}
